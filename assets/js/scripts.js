
/*

Integrantes: Edmar Bila ; Lucas; Pedro Oliveira; Taynan Vitor; Vinicius Evandro; Wellington Siqueira;




4. Armaze as informações no `localStorage` e persista as informações.

    5. BONUS: Armazene as informações de linguagem, presente no parametro `lang`:

        - 4.1. Altere o idioma da saudação

            Exemplo: Olá, Hi, Holá, etc.

            Nota: O intuito é realizar uma prova de conceito, portanto, não se preocupe com a quantidade de idiomas suportados.

*/


   /* Implemente seu algorítmo aqui...
    
        Resumo da atividade:
    
        1. Capture o nome do usuário a partir do campo `<input name="usuario">`:
 */

let input = document.forms['formulario']


input.addEventListener('submit', (evento) =>{
    
    evento.preventDefault()
    
    /* - 1.1. Armazene esse valor em uma variável; */
    var nomeUsuario = evento.target['nome'].value
    /* - 1.2. Redirecione o usuário e envie o valor para rota `resultado/` com parametro `nome`. */
    location.href = `http://127.0.0.1:5500/mesa-de-trabalho/resultado/?nome=${nomeUsuario}`
    
    /* 2. Crie um parametro `lang` e defina o valor `br`. */
    
        
    /* 3. Capture o valor do parametro `usuario`:
    
    - 1.1. Se houver o parametro `usuario` apresente o valor no elemento `<h1 class="texto">`

    - 1.2. Caso contrário apresente apenas a saudação. 
    
    Nota: A saudação já esta presente no código HTML. */
    
    
    var parametros = location.search;
    
    var consulta = new URLSearchParams(parametros);
    
    var existeParametroNome = consulta.has('nome');
    
    if(existeParametroNome) {
        
        var valorDoParametroNome = consulta.get('nome');
    
        var textoNomeDoUsuario = document.createTextNode(valorDoParametroNome);
        
        var titulo = document.getElementsByClassName('texto')
    
        titulo.appendChild(textoNomeDoUsuario);
    
    } 
    else {
    
    /* var textoPadrao = document.createTextNode('aluno(a)');
    
        elementHTML.appendChild(textoPadrao); */
    }
})

